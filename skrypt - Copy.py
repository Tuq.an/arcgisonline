import csv
import pandas as pd
import requests
import json
import subprocess

def get_station_data():
    url = "https://api.gios.gov.pl/pjp-api/rest/station/findAll"
    response = requests.get(url)
    stations = response.json()
    with open("stations.json", "w", encoding="utf-8") as f:
        json.dump(stations, f, indent=2, ensure_ascii=False)

def get_lubelskie_stations():
    with open("stations.json", "r", encoding="utf-8") as file:
        json_data = file.read()
        data = json.loads(json_data)
        lista = []
        lista_id_stacji = []
        for el in data:
            #if el['city']['commune']['provinceName'] == "LUBELSKIE":
            #lista.append(el)
            lista_id_stacji.append(el["id"])

        # sciezka_do_pliku_csv = 'wynik.csv'
        #
        # nazwy_kolumn = ["id", "stationName", "gegrLat", "gegrLon", "city_name", "communeName", "districtName",
        #                 "provinceName", "addressStreet"]
        #
        # with open(sciezka_do_pliku_csv, mode='w', newline='', encoding='utf-8') as file:
        #     writer = csv.writer(file)
        #
        #     writer.writerow(nazwy_kolumn)
        #
        #     for obiekt in lista:
        #         row = [
        #             obiekt["id"],
        #             obiekt["stationName"],
        #             obiekt["gegrLat"],
        #             obiekt["gegrLon"],
        #             obiekt["city"]["name"],
        #             obiekt["city"]["commune"]["communeName"],
        #             obiekt["city"]["commune"]["districtName"],
        #             obiekt["city"]["commune"]["provinceName"],
        #             obiekt["addressStreet"]
        #         ]
        #         writer.writerow(row)
        return lista_id_stacji


def get_sensor_data(station_id):
    # all_sensors = []
    # for el in station_id:
    #     url = f"https://api.gios.gov.pl/pjp-api/rest/station/sensors/{el}"
    #     response = requests.get(url)
    #     sensors = response.json()
    #     all_sensors.extend(sensors)
    #     print(all_sensors)
    #     with open(f"sensors_lubelskie.json", "w", encoding="utf-8") as f:
    #         json.dump(all_sensors, f, indent=2, ensure_ascii=False)
    all_sensors = []
    for el in station_id:
        url = f"https://api.gios.gov.pl/pjp-api/rest/station/sensors/{el}"
        response = requests.get(url)
        sensors = response.json()
        for sensor in sensors:
            if sensor['param']['paramCode'] == 'PM10' or sensor['param']['paramCode'] == 'PM2.5' or sensor['param']['paramCode'] == 'SO2':
                all_sensors.append(sensor['id'])

    #with open(f"sensors_lubelskie_pm10.json", "w", encoding="utf-8") as f:
        #json.dump(pm10_sensors, f, indent=2, ensure_ascii=False)
    return  all_sensors

def get_measurement_data(sensor_ids):
    # url = f"https://api.gios.gov.pl/pjp-api/rest/data/getData/{sensor_id}"
    # response = requests.get(url)
    # measurement_data = response.json()
    # with open(f"measurement_data_{sensor_id}.json", "w", encoding="utf-8") as f:
    #     json.dump(measurement_data, f, indent=2, ensure_ascii=False)
    all_results = []

    for sensor_id in sensor_ids:
        url = f"https://api.gios.gov.pl/pjp-api/rest/data/getData/{sensor_id}"
        response = requests.get(url)
        measurement_data = response.json()

        latest_data = measurement_data["values"][:1]
        if latest_data and latest_data[0].get('value') is not None:
            #latest_data[0]['value'] = round(latest_data[0]['value'], 0)

            result_data = {
                "key": measurement_data["key"],
                "id_x": str(sensor_id),
                "values": latest_data
            }

            all_results.append(result_data)
    filtered_data = [item for item in all_results if item.get('values') and item['values'][0].get('value') is not None]

    # Zapisz wyniki do jednego pliku JSON
    with open("measurement_data_all.json", "w", encoding="utf-8") as f:
        json.dump(filtered_data, f, indent=2, ensure_ascii=False)


def json_to_csv(json_filename, csv_filename):
    with open(json_filename, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)

    # Otwórz plik CSV w trybie zapisu
    with open(csv_filename, 'w', newline='', encoding='utf-8') as csv_file:
        # Utwórz obiekt writer do zapisywania danych do pliku CSV
        csv_writer = csv.writer(csv_file)

        # Zapisz nagłówki do pliku CSV
        headers = ["key", "id_x", "date", "value"]
        csv_writer.writerow(headers)

        # Iteruj po danych JSON i zapisuj je do pliku CSV
        for entry in json_data:
            key = entry["key"]
            id_x = entry["id_x"]

            for value_entry in entry["values"]:
                date = value_entry["date"]
                value = value_entry["value"]

                # Zapisz wiersz danych do pliku CSV
                csv_writer.writerow([key, id_x, date, value])

def polacz_pliki_csv_pandas(plik1, plik2, klucz1, klucz2, plik_wyjsciowy):
    # Wczytaj dane z pierwszego pliku CSV
    df1 = pd.read_csv(plik1)

    # Wczytaj dane z drugiego pliku CSV
    df2 = pd.read_csv(plik2)

    # Połącz dane na podstawie różnych kluczy dla obu ramek danych
    połaczona_ramka = pd.merge(df1, df2, left_on=klucz1, right_on=klucz2)

    # Zapisz połączoną ramkę danych do nowego pliku CSV
    połaczona_ramka.to_csv(plik_wyjsciowy, index=False)

def git_commit_and_push(commit_message):
    try:
        # Dodaj wszystkie zmienione pliki do indeksu
        subprocess.run(["git", "add", "."])

        # Zrób commit z podanym komunikatem
        subprocess.run(["git", "commit", "-m", commit_message])

        # Pushuj zmiany na aktualny branch
        subprocess.run(["git", "push"])

    except Exception as e:
        print(f"Wystąpił błąd: {e}")

tabela_sensorow = [672, 670, 14395, 744, 297, 14397, 618, 621, 54, 101, 224, 25988, 400, 450, 14730, 14731, 14733, 20210, 21370, 21636, 21642,
                   21644, 29567, 29568, 965, 966, 995, 996, 998, 26111, 26117, 27328, 2069, 2071, 2088, 2087, 2199, 2377, 2378, 2382, 16250, 2039,
                   2035, 2222, 2245, 16630, 16633, 21363, 3802, 14379, 17276, 16043, 20365, 28812, 27292, 26261, 26262, 28500, 28523, 16149, 16147,
                   25986, 3843, 3844, 3847, 3902, 3970, 27333, 21683, 26926, 25941, 25940, 26935, 14466, 4391, 20277, 16344, 28486, 28487, 28488, 28489,
                   20236, 20218, 4196, 4198, 26986, 21905, 14416, 25910, 4336, 4338, 4339, 26987, 26989, 5721, 16396, 16214, 16173, 27606, 16178, 29516,
                   29514, 6087, 6085, 20176, 26996, 26997, 6168, 6165, 14972, 5904, 14893, 6108, 14892, 21686, 17945, 17958, 26998, 29267, 29268, 6352, 6354,
                   6382, 6383, 6385, 6400, 29691, 26269, 5286, 5290, 5312, 5317, 5349, 25931, 5376, 5378, 5382, 5480, 5505, 5508, 5466, 5470, 17439, 21228,
                   5167, 5171, 16551, 5232, 5256, 5260, 5515, 5516, 5550, 5547, 5618, 5619, 5623, 14915, 14916, 21303, 20220, 20661, 21083, 20681, 21128, 21129,
                   21130, 26885, 19749, 19753, 19756, 26010, 27027, 5085, 5115, 5116, 5118, 20310, 20311, 17799, 17800, 21047, 26004, 25995, 25997, 29553, 29556,
                   29550, 5766, 5768, 5770, 5683, 5686, 20691, 20796, 20778, 20846, 20847, 2750, 2752, 2770, 2774, 2792, 2794, 2797, 16377, 16786, 16784, 17309, 20320,
                   3073, 3070, 16494, 16457, 2941, 2944, 20324, 20325, 14751, 17184, 17185, 17265, 17271, 17272, 21638, 21639, 4024, 19716, 25908, 25909, 26323, 21243,
                   4079, 4082, 16220, 16130, 19697, 21323, 21324, 4706, 4707, 27667, 4715, 4717, 27567, 4761, 4762, 4764, 26219, 26221, 16071, 4817, 4819, 4845, 4848, 16070,
                   25925, 4898, 3023, 3026, 3058, 21453, 3123, 3127, 3195, 16498, 21489, 21249, 21183, 21571, 27646, 27834, 27835, 28426, 28432, 3584, 3585, 3654, 18038, 3730, 3731,
                   3764, 14688, 18200, 18201, 20411, 18198, 18199, 3418, 3429, 3434, 14376, 3497, 3502, 14377, 3204, 3205, 20461, 14378, 17225, 16044, 16051, 20462, 14779, 3526, 3528, 28895,
                   25983, 25984, 21509, 21510, 1900, 1902, 16472, 20419, 14348, 27126, 2008, 16744, 26316, 18041, 20426, 21787, 21791, 2421, 2425, 26165, 2664, 2668, 14428, 20232, 2510, 2528, 2563,
                   2566, 19721, 2601, 14618, 20221, 853, 1405, 1409, 1426, 21163, 1544, 19698, 20302, 21264, 812, 1650, 6238, 26305, 6218, 6322, 6324, 29522, 29523, 28476, 28477]
#get_station_data()
#id_stacji = get_lubelskie_stations()
#sensors = get_sensor_data(id_stacji)
get_measurement_data(tabela_sensorow)
json_to_csv("measurement_data_all.json", "all_measurment.csv")
polacz_pliki_csv_pandas('sensors_lubelskie_pm10.csv', 'wynik.csv', 'stationId', 'id', 'połaczony_plik.csv')
polacz_pliki_csv_pandas('all_measurment.csv', 'połaczony_plik.csv', 'id_x', 'id_x', 'polaczony_plik_final.csv')
git_commit_and_push("upload_danych")
