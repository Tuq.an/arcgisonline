import csv
import pandas as pd
import requests
import json
import subprocess

def get_station_data():
    url = "https://api.gios.gov.pl/pjp-api/rest/station/findAll"
    response = requests.get(url)
    stations = response.json()
    with open("stations.json", "w", encoding="utf-8") as f:
        json.dump(stations, f, indent=2, ensure_ascii=False)

def get_lubelskie_stations():
    with open("stations.json", "r", encoding="utf-8") as file:
        json_data = file.read()
        data = json.loads(json_data)
        lista = []
        lista_id_stacji = []
        for el in data:
            lista_id_stacji.append(el["id"])

        sciezka_do_pliku_csv = 'wynik.csv'

        nazwy_kolumn = ["id", "stationName", "gegrLat", "gegrLon", "city_name", "communeName", "districtName",
                        "provinceName", "addressStreet"]

        with open(sciezka_do_pliku_csv, mode='w', newline='', encoding='utf-8') as file:
            writer = csv.writer(file)

            writer.writerow(nazwy_kolumn)

            for obiekt in lista:
                row = [
                    obiekt["id"],
                    obiekt["stationName"],
                    obiekt["gegrLat"],
                    obiekt["gegrLon"],
                    obiekt["city"]["name"],
                    obiekt["city"]["commune"]["communeName"],
                    obiekt["city"]["commune"]["districtName"],
                    obiekt["city"]["commune"]["provinceName"],
                    obiekt["addressStreet"]
                ]
                writer.writerow(row)
        return lista_id_stacji


def get_sensor_data(station_id):
    all_sensors = []
    for el in station_id:
        url = f"https://api.gios.gov.pl/pjp-api/rest/station/sensors/{el}"
        response = requests.get(url)
        sensors = response.json()
        all_sensors.extend(sensors)
        print(all_sensors)
        with open(f"sensors_lubelskie.json", "w", encoding="utf-8") as f:
            json.dump(all_sensors, f, indent=2, ensure_ascii=False)
    all_sensors = []
    for el in station_id:
        url = f"https://api.gios.gov.pl/pjp-api/rest/station/sensors/{el}"
        response = requests.get(url)
        sensors = response.json()
        for sensor in sensors:
            if sensor['param']['paramCode'] == 'PM10' or sensor['param']['paramCode'] == 'PM2.5' or sensor['param']['paramCode'] == 'SO2':
                all_sensors.append(sensor['id'])

    return  all_sensors

def get_measurement_data(sensor_ids):
    all_results = []

    for sensor_id in sensor_ids:
        url = f"https://api.gios.gov.pl/pjp-api/rest/data/getData/{sensor_id}"
        response = requests.get(url)
        measurement_data = response.json()

        latest_data = measurement_data["values"][:1]
        if latest_data and latest_data[0].get('value') is not None:

            result_data = {
                "key": measurement_data["key"],
                "id_x": str(sensor_id),
                "values": latest_data
            }

            all_results.append(result_data)
    filtered_data = [item for item in all_results if item.get('values') and item['values'][0].get('value') is not None]

    with open("measurement_data_all.json", "w", encoding="utf-8") as f:
        json.dump(filtered_data, f, indent=2, ensure_ascii=False)


def json_to_csv(json_filename, csv_filename):
    with open(json_filename, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)

    with open(csv_filename, 'w', newline='', encoding='utf-8') as csv_file:
        csv_writer = csv.writer(csv_file)

        headers = ["key", "id_x", "date", "value"]
        csv_writer.writerow(headers)

        for entry in json_data:
            key = entry["key"]
            id_x = entry["id_x"]

            for value_entry in entry["values"]:
                date = value_entry["date"]
                value = value_entry["value"]
                csv_writer.writerow([key, id_x, date, value])

def polacz_pliki_csv_pandas(plik1, plik2, klucz1, klucz2, plik_wyjsciowy):
    df1 = pd.read_csv(plik1)
    df2 = pd.read_csv(plik2)
    połaczona_ramka = pd.merge(df1, df2, left_on=klucz1, right_on=klucz2)
    połaczona_ramka.to_csv(plik_wyjsciowy, index=False)

def git_commit_and_push(commit_message):
    try:
        subprocess.run(["git", "add", "."])

        subprocess.run(["git", "commit", "-m", commit_message])

        subprocess.run(["git", "push"])

    except Exception as e:
        print(f"Wystąpił błąd: {e}")


get_station_data()
id_stacji = get_lubelskie_stations()
sensors = get_sensor_data(id_stacji)
get_measurement_data(sensors)
json_to_csv("measurement_data_all.json", "all_measurment.csv")
polacz_pliki_csv_pandas('sensors_lubelskie_pm10.csv', 'wynik.csv', 'stationId', 'id', 'połaczony_plik.csv')
polacz_pliki_csv_pandas('all_measurment.csv', 'połaczony_plik.csv', 'id_x', 'id_x', 'polaczony_plik_final.csv')
git_commit_and_push("upload_danych")
